import express from "express";
import httpStatusCodes from "http-status-codes";
import userService from "../../services/user-services.js";

const router = express.Router();

router.get("/", async (req, res, next) => {
    let result;
      try {
        result = await userService.getUsers();
        return res.status(httpStatusCodes.OK).send(result);
    } catch (error) {
        throw new Error(error);
    }
});


router.get("/error", async (req, res, next) => {
    let result;
      try {
        result = await userService.getUsersFails();
        return res.status(httpStatusCodes.OK).send(result);
    } catch (error) {
        return res.status(error.errorCode).send(error.message);
    }
});

export default router;
