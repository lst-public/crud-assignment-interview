import httpStatusCodes from "http-status-codes";


function errorResponse(value, message, errorCode) {
    return {
        value,
        message,
        errorCode,
     }
}
// Error Messages
function NotFoundError(message) {
    this.value = "";
    this.message = message;
    this.errorCode = httpStatusCodes.NOT_FOUND;
    errorResponse(this.value, this.message, this.errorCode);
}


export default {
    NotFoundError
  };
  