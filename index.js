import http from "http";
import express from "express";
import config from "config";
import bearerToken from "express-bearer-token";
import users from "./routes/v1/user.js";
import index from "./routes/v1/index.js";

const app = express();

app.use(express.json());
app.use(bearerToken());

app.use("/", index);

app.use("/v1/users", users);

const port = config.servicePort || 3000;
app.listen(port, () => {
    console.log(`Server runnning on port: ${port}`);
});

export default app;